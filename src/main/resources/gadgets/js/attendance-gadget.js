(function($) {
    AJS.$.namespace("attendance.gadget.overview");


    attendance.gadget.overview.templateArgs = function(baseUrl) {
        return [{
            key: "result",
            ajaxOptions: function(){
                return {
                    url: baseUrl + "/rest/attendance/1.0/status/all"
                }
            }

        }];

    };


    attendance.gadget.overview.template = function (gadget, args, baseUrl) {
        AJS.$("body").append(attendance.template.userTable());
        for(i in args) {
            if(args[i].username!=null) {
                args[i].baseUrl= baseUrl;
                var template = attendance.template.user(args[i]);
                AJS.$("#attendance-table").append(template);
            }
        }
    };

    attendance.gadget.overview.init = function(baseUrl) {
        AJS.Gadget({
            baseUrl: baseUrl,
            useOauth: "/rest/gadget/1.0/currentUser",
//            config: {
//
//            },
            view: {
                enableReload: true,
                onResizeAdjustHeight: true,
                template: function (args) {
                    return attendance.gadget.overview.template(this, args.result, baseUrl);
                },
                args: attendance.gadget.overview.templateArgs(baseUrl)
            }
        });
    }
})(jQuery);

