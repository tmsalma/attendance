(function($) {
    AJS.$.namespace("attendance.me.gadget.overview");


    attendance.me.gadget.overview.templateArgs = function(baseUrl) {
        return [{
            key: "result",
            ajaxOptions: function(){
                return {
                    url: baseUrl + "/rest/attendance/1.0/status/me"
                }
            }

        }];

    };


    attendance.me.gadget.overview.template = function (gadget, args, baseUrl) {
        AJS.$("body").append(attendance.me.template.userTable());
        for(i in args) {
            if(args[i].username!=null && i<10) {
                args[i].baseUrl= baseUrl;
                var date = new Date(args[i].statusDate);
                m_names = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September","October", "November", "December");
                args[i].formattedDate = date.getDate()+". " + m_names[(date.getMonth())] +"  " + date.getHours()+":" + date.getMinutes();
                var template = attendance.me.template.user(args[i]);
                AJS.$("#attendance-table").append(template);
            }
        }
    };

    attendance.me.gadget.overview.init = function(baseUrl) {
        AJS.Gadget({
            baseUrl: baseUrl,
            useOauth: "/rest/gadget/1.0/currentUser",
            view: {
                enableReload: true,
                onResizeAdjustHeight: true,
                template: function (args) {
                    return attendance.me.gadget.overview.template(this, args.result, baseUrl);
                },
                args: attendance.me.gadget.overview.templateArgs(baseUrl)
            }
        });
    }
})(jQuery);

