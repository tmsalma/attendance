if (typeof Attendance == 'undefined') { var Attendance = {}; }

Attendance.instances = {};

Attendance.Line = Backbone.Model.extend({
    url : contextPath + "/rest/attendance/1.0/status/",
    defaults : {
        contextPath : contextPath
    }


});


Attendance.LineList = Backbone.Collection.extend({
    model: Attendance.Line,

    initialize : function(){
        this.url = contextPath + "/rest/attendance/1.0/status/statusList"
    },

    parse: function(response){
        return response;
    }
});

Attendance.LineView = Backbone.View.extend({
    className: "line-row",

    render: function() {
        var output = Attendance.template.lineItem(this.model.toJSON());
        this.$el.html(output);
        return this;
    }

});


Attendance.AppView = Backbone.View.extend({
    events: {
        "click .button-ok":  "updateStatus"
    },

    initialize: function() {

        this.statusComment = this.$(".description");
        this.main = this.$('.attendance-list');

        this.model.bind('reset', this.addAll, this);
        this.model.bind('all', this.render, this);


        this.model.fetch();
    },


    render: function() {
        return this;
    },

    updateStatus : function(e){
        var description = this.statusComment.val();
        var statusType = this.$(":checked").val();

        this.model.create({statusType:statusType, description: description});
        jQuery("#aui-dialog-close").click();
        refreshStatusIcon();
    },


    addOne: function(line) {
        var view = new Attendance.LineView({model: line});
        this.main.append(view.render().el);
    },

    addAll: function() {
        this.model.each(this.addOne, this);
    }

});




