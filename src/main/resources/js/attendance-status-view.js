JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function (e, context) {


//    var createItem = jQuery('#createItem');
//    createItem.parent().parent().append('<li id="attendance-status" class="aui-dd-parent">Jin&Jout</li>');

    var TempoMediumPopupForm = new AJS.FormPopup({
        id:"attendance-medium-form",
        trigger:"a#attendance_menu",
        ajaxOptions:{
            data:{
                inline:true,
                decorator:"dialog"
            }
        },
        initCallback:function () {
            hideFunction = this.hide;
        },
        widthClass:"medium",
        onContentRefresh:function () {
            var context = this.get$popupContent();
//            console.log("context:");
//            console.log(context);

//            console.log("document ready");

            var $container = AJS.$("#attendance-update-status");

            var appTemplate = Attendance.template.attendanceApp();

            $container.html(appTemplate);

            var list = new Attendance.LineList();

            var app = new Attendance.AppView({model: list, el: "#attendance-update-status"});

//            Attendance.instances['lulli'] = app;


        }
    });

    var li = AJS.$('#header-details-user');
    if (AJS.$('#header-details-user-fullname').length > 0) {
        //user is logged in get his status
        var username = jQuery('#header-details-user-fullname').attr('data-username');
        li.parent().append('<li id="attendance-status"></li>');
        var statusDiv = AJS.$('#attendance-status');
        statusDiv.append('<div id="statusSpan"/>');
        refreshStatusIcon();

        jQuery("#statusSpan").click(function() {
            jQuery("#attendance_menu").click();
        });


    }
});

function refreshStatusIcon() {
    AJS.$.ajax({
        url: contextPath + '/rest/attendance/1.0/status/me/current',
        dataType: 'json',
        cache : 'false',
        success: function(data) {
            AJS.$('#statusSpan').html("<img width='20px' alt='" + data.description + "' title='" + data.statusDescription + "/" +  data.description + "' src='" + contextPath + "/download/resources/is.tmsoftware.jira.attendance:attendance-gadget-overview/img/" + data.statusType + ".png'>");
        }
    });
}
