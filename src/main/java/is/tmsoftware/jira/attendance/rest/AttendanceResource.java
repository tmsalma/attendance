package is.tmsoftware.jira.attendance.rest;

/**
 * Created with IntelliJ IDEA.
 * User: bjarnit
 * Date: 11/1/12
 * Time: 1:30 PM
 */

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.google.common.collect.Lists;
import is.tmsoftware.jira.attendance.Status;
import is.tmsoftware.jira.attendance.StatusManager;
import is.tmsoftware.jira.attendance.StatusModel;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;

@Path("/status")
public class AttendanceResource {

    private final StatusManager statusManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public AttendanceResource(StatusManager statusManager,
                              JiraAuthenticationContext jiraAuthenticationContext) {
        this.statusManager = statusManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @GET
    @Path("/all")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getStatusForAll()
    {
        return Response.ok(statusManager.getStatusForAll()).build();
    }

    @GET
    @Path("/me")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getMyStatuses()
    {
        return Response.ok(statusManager.getAllForUser(jiraAuthenticationContext.getLoggedInUser())).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response postStatus(StatusModel statusModel) {
        statusManager.addStatus(statusModel);
        return Response.ok().build();
    }

    @GET
    @Path("/statusList")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getStatusList()
    {
        Collection<Status> allStatuses = Lists.newArrayList();
        for (Status.StatusType status : Status.StatusType.values()) {
            allStatuses.add(new Status(status, jiraAuthenticationContext.getI18nHelper()));
        }
        return Response.ok(allStatuses).build();
    }

    @GET
    @Path("/me/current")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getMyCurrentStatuses(){
        return Response.ok(statusManager.getCurrentForUser(jiraAuthenticationContext.getLoggedInUser())).build();
    }


}