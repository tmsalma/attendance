package is.tmsoftware.jira.attendance;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.util.UserManager;
import is.tmsoftware.jira.attendance.ao.StatusService;

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: bjarnit
 * Date: 11/1/12
 * Time: 1:51 PM
 */
public class StatusManager {

    private final StatusService aoStatusService;
    private final UserManager userManager;
    private final AvatarService avatarService;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public StatusManager(StatusService aoStatusService, UserManager userManager, AvatarService avatarService, JiraAuthenticationContext jiraAuthenticationContext) {
        this.aoStatusService = aoStatusService;
        this.userManager = userManager;
        this.avatarService = avatarService;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    public Collection<StatusModel> getStatusForAll() {
        return populateModels(aoStatusService.all());
    }

    private Collection<StatusModel> populateModels(Collection<StatusModel> statusModels) {
        for (StatusModel statusModel : statusModels) {
            populateModel(statusModel);
        }
        return statusModels;
    }

    private StatusModel populateModel(StatusModel statusModel) {
        if (statusModel == null) {
            statusModel = new StatusModel();
            statusModel.setUsername(jiraAuthenticationContext.getLoggedInUser().getName());
            statusModel.setStatusType(Status.StatusType.out);
        }
        User user = userManager.getUser(statusModel.getUsername());
        if (user != null && user.getDisplayName() != null) {
            statusModel.setUserFullName(user.getDisplayName());
            statusModel.setUserAvatarId(getAvatarId(user));
        }
        else {
            statusModel.setUserFullName(statusModel.getUsername());
        }
        if (statusModel.getStatusType() != null) {
            Status status = new Status(statusModel.getStatusType(), jiraAuthenticationContext.getI18nHelper());
            statusModel.setStatusDescription(status.getStatusDesc());
        }
        return statusModel;
    }


    public long getAvatarId(User user) {
        if (user != null) {
            Avatar avatar = avatarService.getAvatar(jiraAuthenticationContext.getLoggedInUser(), user.getName());
            if (avatar != null) {
                return avatar.getId();
            }
        }
        return 0L;
    }

    public void addStatus(StatusModel statusModel) {
        aoStatusService.add(statusModel);
    }

    public Collection<StatusModel> getAllForUser(User loggedInUser) {
        return populateModels(aoStatusService.allForUser(loggedInUser.getName()));
    }

    public StatusModel getCurrentForUser(User loggedInUser) {
        return populateModel(aoStatusService.latestForUser(loggedInUser.getName()));
    }
}
