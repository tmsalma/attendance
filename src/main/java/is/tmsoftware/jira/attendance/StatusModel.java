package is.tmsoftware.jira.attendance;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: kristin
 * Date: 11/1/12
 * Time: 1:43 PM
 */

@XmlRootElement(name = "user-status")
@XmlAccessorType(XmlAccessType.FIELD)
public class StatusModel {
    @XmlElement
    private String username;
    @XmlElement
    private Date statusDate;
    @XmlElement
    private Status.StatusType statusType;
    @XmlElement
    private String description;
    @XmlElement
    private String comment;
    @XmlElement
    private String userFullName;
    @XmlElement
    private long userAvatarId;
    @XmlElement
    private String statusDescription;

   private String contextPath;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public Status.StatusType getStatusType() {
        return statusType;
    }

    public void setStatusType(Status.StatusType statusType) {
        this.statusType = statusType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public long getUserAvatarId() {
        return userAvatarId;
    }

    public void setUserAvatarId(long userAvatarId) {
        this.userAvatarId = userAvatarId;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getContextPath() {
        return contextPath;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }
}
