package is.tmsoftware.jira.attendance.ao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.security.JiraAuthenticationContext;
import is.tmsoftware.jira.attendance.InfoModel;
import is.tmsoftware.jira.attendance.StatusModel;
import net.java.ao.Query;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: magnusg
 * Date: 26.11.2012
 * Time: 10:48
 * To change this template use File | Settings | File Templates.
 */
public class InfoServiceImpl implements InfoService{
    private final ActiveObjects ao;
    private final JiraAuthenticationContext jiraAuthenticationContext;


    public InfoServiceImpl(ActiveObjects ao, JiraAuthenticationContext jiraAuthenticationContext) {
        this.ao = ao;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @Override
    public List<InfoModel> allForUser(String username) {
        List<InfoModel> userInfoList = new ArrayList<InfoModel>();
        if (username == null) {
            return userInfoList;
        }

        UserInfo[] list = ao.find(UserInfo.class, Query.select().where("USERNAME = ? ", username));
        for (UserInfo uiInfo: list)  {
            userInfoList.add(getModel(uiInfo));
        }
        return userInfoList;
    }

    @Override
    public InfoModel latestForUser(String username) {
        List<InfoModel> all = allForUser(username);
        InfoModel currInfo = null;
        for (InfoModel current : all ) {
            if (currInfo == null) {
                currInfo = current;
            }
        }
        return currInfo;
    }


    private InfoModel getModel(UserInfo aoInfo) {
        InfoModel info = new InfoModel();
        info.setUsername(aoInfo.getUserName());
        info.setCompany(aoInfo.getUserCompany());
        info.setUserAddress(aoInfo.getUserAddress());
        info.setUseremail(aoInfo.getUserEmail());
        info.setUserHomePhone(aoInfo.getUserHomePhone());
        info.setUserMobilePhone(aoInfo.getUserMobilePhone());
        info.setUserSocialNumber(aoInfo.getUserSocialNumber());
        info.setUserWorkPhone(aoInfo.getUserWorkPhone());
        return info;
    }

    /**
     * Get Info Model for all users
     * */
    @Override
    public List<InfoModel> all() {
        UserInfo[] uiInfo = ao.find(UserInfo.class);
        Map<String, InfoModel> userMap = new HashMap<String, InfoModel>();
        for (UserInfo recInfo: uiInfo) {
            InfoModel model = getModel(recInfo);
            userMap.put(model.getUsername(), model);
        }
        return new ArrayList<InfoModel>(userMap.values());
    }

    private boolean isNewStatus(Map<String, StatusModel> userStatus, StatusModel model) {
        if (userStatus.containsKey(model.getUsername())) {
            StatusModel currentStatus = userStatus.get(model.getUsername());
            return currentStatus.getStatusDate() == null || currentStatus.getStatusDate().before(model.getStatusDate());
        }
        return true;
    }

    @Override
    public UserInfo add(InfoModel model) {
        UserInfo uiInfo = ao.create(UserInfo.class);
        uiInfo.setUsername((String) getValue(model.getUsername(), jiraAuthenticationContext.getLoggedInUser().getName()));
        uiInfo.setUserAddress(model.getUserAddress());
//        uiInfo.setUserCompany(model.getUserCompany());
        uiInfo.setUserHomePhone(model.getUserHomePhone());
        uiInfo.setUserMobilePhone(model.getUserMobilePhone());
        uiInfo.setUserSocialNumber(model.getUserSocialNumber());
        uiInfo.setUserWorkPhone(model.getUserWorkPhone());
        uiInfo.save();
        return uiInfo;
    }

    private Object getValue(Object value, Object defaultValue) {
        if (value == null) {
            return defaultValue;
        }
        return value;
    }
}
