package is.tmsoftware.jira.attendance.ao;

import com.atlassian.activeobjects.tx.Transactional;
import is.tmsoftware.jira.attendance.InfoModel;


import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: mg
 * Date: 11/1/12
 * Time: 1:23 PM
 */
@Transactional
public interface InfoService {

    List<InfoModel> allForUser(String username);

    InfoModel latestForUser(String username);

    List<InfoModel> all();

    UserInfo add(InfoModel model);
}
