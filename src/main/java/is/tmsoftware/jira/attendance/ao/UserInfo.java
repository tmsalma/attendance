package is.tmsoftware.jira.attendance.ao;

import net.java.ao.Entity;
import net.java.ao.schema.Table;

/**
 * Created with IntelliJ IDEA.
 * User: magnusg
 * Date: 23.11.2012
 * Time: 13:27
 * To change this template use File | Settings | File Templates.
 */

@Table("USER_INFO")
public interface UserInfo extends Entity {

    String getUserName();
    void setUsername(String userName);

    String getUserEmail();
    void setUserEmail(String userEmail);

    String getUserSocialNumber();
    void setUserSocialNumber(String userSocialNumber);

    String getUserAddress();
    void setUserAddress(String userAddress);

    String getUserWorkPhone();
    void setUserWorkPhone(String userWorkPhone);

    String getUserHomePhone();
    void setUserHomePhone(String userHomePhone);

    String getUserMobilePhone();
    void setUserMobilePhone(String userMobilePhone);

    String getUserCompany();
    void setUserCompany(String userCompany);

}
