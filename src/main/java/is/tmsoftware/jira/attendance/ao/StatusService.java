package is.tmsoftware.jira.attendance.ao;

import com.atlassian.activeobjects.tx.Transactional;
import is.tmsoftware.jira.attendance.StatusModel;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: kristin
 * Date: 11/1/12
 * Time: 1:23 PM
 */
@Transactional
public interface StatusService {

    List<StatusModel> allForUser(String username);

    StatusModel latestForUser(String username);

    List<StatusModel> all();

    UserStatus add(StatusModel model);
}
