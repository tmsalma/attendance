package is.tmsoftware.jira.attendance.ao;

import is.tmsoftware.jira.attendance.Status;
import net.java.ao.Entity;
import net.java.ao.schema.Table;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: kristin
 * Date: 11/1/12
 * Time: 12:35 PM
 */
@Table("USER_STATUS")
public interface UserStatus extends Entity {

    String getUsername();
    void setUsername(String username);

    Date getStatusDate();
    void setStatusDate(Date statusDate);

    Status.StatusType getStatus();
    void setStatus(Status.StatusType status);

    String getDescription();
    void setDescription(String description);

    String getComment();
    void setComment(String comment);

}
