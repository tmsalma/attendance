package is.tmsoftware.jira.attendance.ao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.security.JiraAuthenticationContext;
import is.tmsoftware.jira.attendance.Status;
import is.tmsoftware.jira.attendance.StatusModel;
import net.java.ao.Query;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: kristin
 * Date: 11/1/12
 * Time: 1:25 PM
 */
public class StatusServiceImpl implements StatusService {

    private final ActiveObjects ao;
    private final JiraAuthenticationContext jiraAuthenticationContext;


    public StatusServiceImpl(ActiveObjects ao, JiraAuthenticationContext jiraAuthenticationContext) {
        this.ao = ao;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }


    @Override
    public List<StatusModel> allForUser(String username) {
        List<StatusModel> userHistory = new ArrayList<StatusModel>();
        if (username == null) {
            return userHistory;
        }

        UserStatus[] list = ao.find(UserStatus.class, Query.select().where("USERNAME = ? ", username).order("STATUS_DATE DESC"));
        for (UserStatus status: list)  {
            userHistory.add(getModel(status));
        }
        return userHistory;
    }

    @Override
    public StatusModel latestForUser(String username) {
        List<StatusModel> all = allForUser(username);
        StatusModel latestStatus = null;
        for (StatusModel current : all ) {
            if (latestStatus == null || current.getStatusDate().after(latestStatus.getStatusDate())) {
                latestStatus = current;
            }
        }
        return latestStatus;
    }


    private StatusModel getModel(UserStatus aoStatus) {
        StatusModel model = new StatusModel();
        model.setUsername(aoStatus.getUsername());
        model.setStatusDate(aoStatus.getStatusDate());
        model.setStatusType(aoStatus.getStatus());
        model.setDescription(aoStatus.getDescription());
        model.setComment(aoStatus.getComment());
        return model;
    }

    /**
     * Get latest status for each user
     * */
    @Override
    public List<StatusModel> all() {
        UserStatus[] statuses = ao.find(UserStatus.class);
        Map<String, StatusModel> userMap = new HashMap<String, StatusModel>();
        for (UserStatus status: statuses) {
            StatusModel model = getModel(status);
            if (isNewStatus(userMap, model)) {
                userMap.put(model.getUsername(), model);
            }
        }
        return new ArrayList<StatusModel>(userMap.values());
    }

    private boolean isNewStatus(Map<String, StatusModel> userStatus, StatusModel model) {
        if (userStatus.containsKey(model.getUsername())) {
            StatusModel currentStatus = userStatus.get(model.getUsername());
            return currentStatus.getStatusDate() == null || currentStatus.getStatusDate().before(model.getStatusDate());
        }
        return true;
    }

    @Override
    public UserStatus add(StatusModel model) {
        UserStatus userStatus = ao.create(UserStatus.class);
        userStatus.setUsername((String) getValue(model.getUsername(), jiraAuthenticationContext.getLoggedInUser().getName()));
        userStatus.setStatusDate((Date) getValue(model.getStatusDate(), new Date()));
        userStatus.setStatus((Status.StatusType) getValue(model.getStatusType(), Status.StatusType.in));
        userStatus.setDescription(model.getDescription());
        userStatus.setComment(model.getComment());
        userStatus.save();
        return userStatus;
    }

    private Object getValue(Object value, Object defaultValue) {
        if (value == null) {
            return defaultValue;
        }
        return value;
    }
}
