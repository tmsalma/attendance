package is.tmsoftware.jira.attendance;

import com.atlassian.jira.util.I18nHelper;

import javax.xml.bind.annotation.*;

/**
 * Created with IntelliJ IDEA.
 * User: bjarnit
 * Date: 11/1/12
 * Time: 3:59 PM
 */
@XmlRootElement(name = "status")
@XmlAccessorType(XmlAccessType.FIELD)
public class Status {
    @XmlAttribute
    private StatusType status;
    @XmlElement
    private String statusDesc;

    public Status(StatusType status, I18nHelper i18nHelper) {
        this.status = status;
        this.statusDesc = i18nHelper.getText("attendance.status." + status.name());
    }

    public StatusType getStatus() {
        return status;
    }

    public void setStatus(StatusType status) {
        this.status = status;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public static enum StatusType {
        in,
        out,
        away,
        workingOffsite,
        sick,
        sickChild,
        vacation
    }
}
