package is.tmsoftware.jira.attendance;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created with IntelliJ IDEA.
 * User: kristin
 * Date: 11/1/12
 * Time: 1:43 PM
 */

@XmlRootElement(name = "user-info")
@XmlAccessorType(XmlAccessType.FIELD)
public class InfoModel {

    @XmlElement
    private String username;
    @XmlElement
    private String useremail;
    @XmlElement
    private String usersocialnumber;
    @XmlElement
    private String useraddress;
    @XmlElement
    private String userworkphone;
    @XmlElement
    private String usermobilephone;
    @XmlElement
    private String userhomephone;
    @XmlElement
    private String usercompany;

    private String contextPath;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }

    public String getUseremail() {
        return useremail;
    }

    public void setUserSocialNumber(String usersocialnumber) {
        this.usersocialnumber = usersocialnumber;
    }

    public String getUserSocialNumber() {
        return usersocialnumber;
    }

    public void setUserAddress(String useraddress) {
        this.useraddress = useraddress;
    }

    public String getUserAddress() {
        return useraddress;
    }

    public void setUserWorkPhone(String userworkphone) {
        this.userworkphone = userworkphone;
    }

    public String getUserWorkPhone() {
        return userworkphone;
    }

    public void setUserHomePhone(String userhomephone) {
        this.userhomephone = userhomephone;
    }

    public String getUserHomePhone() {
        return userworkphone;
    }

    public void setUserMobilePhone(String usermobilephone) {
        this.usermobilephone = usermobilephone;
    }

    public String getUserMobilePhone() {
        return usermobilephone;
    }

    public void setCompany(String usercompany) {
        this.usercompany = usercompany;
    }

    public String getUserCompany() {
        return usercompany;
    }

    public String getContextPath() {
        return contextPath;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }
}