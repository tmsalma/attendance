package is.tmsoftware.jira.attendance.plugin;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.user.UserUtils;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import is.tmsoftware.jira.attendance.ao.InfoServiceImpl;

import java.util.Collection;

/**
 * Created by IntelliJ IDEA.
 * User: dadinn
 * Date: 23.11.2012
 * Time: 09:47
 * To change this template use File | Settings | File Templates.
 */
public class AllUsersReport extends JiraWebActionSupport {

    private String userName;
    private InfoServiceImpl infoService;

    public AllUsersReport(InfoServiceImpl infoService) {
        this.infoService = infoService;
    }

    public Collection<User> getAvailableUsers() {
        // Add user to db
//        InfoModel infoModel = new InfoModel();
//        infoModel.setUsername("admin");
//        infoModel.setUserAddress("Laugarvegur xx");
//        infoModel.setUserHomePhone("5346766");
//        infoModel.setUserMobilePhone("8480562");
//        infoModel.setUserSocialNumber("0503794509");
//        infoModel.setUserWorkPhone("123456");
//        infoService.add(infoModel);
        //
        return UserUtils.getAllUsers();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}

