package is.tmsoftware.jira.attendance.plugin;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import is.tmsoftware.jira.attendance.InfoModel;
import is.tmsoftware.jira.attendance.ao.InfoServiceImpl;

/**
 * Created by IntelliJ IDEA.
 * User: dadinn
 * Date: 23.11.2012
 * Time: 09:47
 * To change this template use File | Settings | File Templates.
 */
public class Admin extends JiraWebActionSupport {

    private InfoServiceImpl infoService;

    public Admin(InfoServiceImpl infoService) {
        this.infoService = infoService;
    }

    public InfoModel userInfo() {
        return infoService.latestForUser("admin");
    }

    public void saveInfo() {
        infoService.all();
    }

}

